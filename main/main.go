package main

import (
	"fmt"
	"hydra/appliances"
)

func main() {
	fmt.Println("Enter preferred appliance type")
	fmt.Println("0: Stove")
	fmt.Println("1: Fridge")
	fmt.Println("2: Microwave")

	var applianceType int
	fmt.Scan(&applianceType)

	appliance, err := appliances.CreateAppliance(applianceType)

	if err == nil {
		appliance.Start()
		fmt.Println(appliance.GetPurpose())
	} else {
		fmt.Println(err)
	}
}
