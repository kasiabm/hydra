package main

import (
	"fmt"
	"net/http"
)

func main() {
	http.HandleFunc("/", sRoot)
	http.ListenAndServe(":8080", nil)
}

func sRoot(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to the Hydra software system")
}
