package appliances

type Stove struct {
	typeName string
}

func (stove *Stove) Start() {
	stove.typeName = "Stove"
}

func (stove *Stove) GetPurpose() string {
	return stove.typeName + " for cooking food"
}
