package appliances

type Microwave struct {
	typeName string
}

func (microwave *Microwave) Start() {
	microwave.typeName = "Microwave"
}

func (microwave *Microwave) GetPurpose() string {
	return microwave.typeName + " for quickly heating food up"
}
